﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public void onRestartClick()
    {
        SceneManager.LoadScene(2);
    }

    public void onExitClick()
    {
        Application.Quit();
    }
}
