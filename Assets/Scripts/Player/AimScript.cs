﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScript : MonoBehaviour
{
    private Animator animator;
    private static readonly int shootHash = Animator.StringToHash("Shoot");

    public GameObject aimCam;
    public GameObject mainCam;
    public GameObject mira;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            animator.SetBool("IsAiming", true);
            mainCam.SetActive(false);
            aimCam.SetActive(true);
            mira.SetActive(true);
        }
        else
        {
            animator.SetBool("IsAiming", false);
            mainCam.SetActive(true);
            aimCam.SetActive(false);
            mira.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        animator.SetTrigger(shootHash);
    }
}
