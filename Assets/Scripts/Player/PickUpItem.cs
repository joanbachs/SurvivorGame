﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PickUpItem : MonoBehaviour
{
    [SerializeField] private GameObject currentObject = null;
    [SerializeField] private GameObject [] pickeableObjects;
    private Animator animator;

    private AudioSource AS;
    [SerializeField] private AudioClip audioClip;


    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        AS = gameObject.GetComponent<AudioSource>();
        AS.clip = audioClip;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            pickUp();
        }

        if (currentObject != null)
        {
            if (currentObject.CompareTag("Killable"))
            {
                gameObject.GetComponent<Health>().Alive = false;
            }
        }
    }

    void pickUp()
    {
        if (currentObject != null)
        {
            StartCoroutine(PickUpCoroutine());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        /*if (other.gameObject.CompareTag("Pickeable"))
        {
            currentObject = other.gameObject;
        }*/

        IPickable ipick = other.gameObject.GetComponent<IPickable>();
        if (ipick != null)
        {
            currentObject = other.gameObject;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        /*if (other.gameObject.CompareTag("Pickeable"))
        {
            currentObject = null;
        }*/

        IPickable ipick = other.gameObject.GetComponent<IPickable>();
        if (ipick != null)
        {
            currentObject = null;
        }
    }

    IEnumerator PickUpCoroutine()
    {
        animator.SetBool("IsPicking", true);
        yield return new WaitForSeconds(0.75f);
        AS.Play();
        yield return new WaitForSeconds(0.75f);
        var pickedObject = GameObject.Find(currentObject.name);
        foreach (GameObject objet in pickeableObjects)
        {
            if (objet.name.Equals(pickedObject.name))
            {
                objet.SetActive(true);
            }

            if (pickedObject.name.Equals("key"))
            {
                GameManager.Instance.AccesToRestrictedArea = true;
            }
        }
        Destroy(currentObject);
        animator.SetBool("IsPicking", false);
    }
}
