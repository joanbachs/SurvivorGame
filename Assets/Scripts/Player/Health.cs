﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    [SerializeField]private bool alive;

    public bool Alive { get => alive; set => alive = value; }

    // Start is called before the first frame update
    void Start()
    {
        //alive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!alive)
        {
            Debug.Log("You died");
            SceneManager.LoadScene(1);
            //Destroy(gameObject);
        }
    }
}
