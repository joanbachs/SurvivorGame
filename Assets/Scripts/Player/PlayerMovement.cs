﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    private Animator animator;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private float playerSpeed = 14f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;

    private bool crouched;
    private bool isDancing;

    public GameObject danceCamera;
    public GameObject mainCam;

    private Vector3 rotation;
    public float _rotationSpeed = 180;

    void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
        animator = gameObject.GetComponent<Animator>();
        crouched = false;
        isDancing = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            isDancing = true;
        }


        if (!isDancing)
        {
            groundedPlayer = controller.isGrounded;

            if (groundedPlayer && playerVelocity.y < 0)
            {
                playerVelocity.y = 0f;
            }
            //Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            //controller.Move(move * Time.deltaTime * playerSpeed);

            //rotation & movement
            this.rotation = new Vector3(0, Input.GetAxis("Horizontal") * _rotationSpeed * Time.deltaTime, 0);
            //Vector3 move = new Vector3(0, 0, Input.GetAxis("Vertical") * Time.deltaTime);

            Vector3 move = new Vector3(0, 0, 0 * Time.deltaTime);

            if (Input.GetKey(KeyCode.W))
            {
                move = new Vector3(0, 0, 1 * Time.deltaTime);
            }
            move = this.transform.TransformDirection(move);
            controller.Move(move * playerSpeed);
            this.transform.Rotate(this.rotation);

            if (move != Vector3.zero)
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    controller.Move(move * Time.deltaTime * (playerSpeed * 2));
                    gameObject.transform.forward = move;
                    animator.SetFloat("Blend", 2);
                }
                else
                {
                    if (crouched)
                    {
                        animator.SetInteger("condition", 1);
                    }
                    gameObject.transform.forward = move;
                    animator.SetFloat("Blend", 1);
                }
            }
            else
            {
                if (crouched)
                {
                    animator.SetInteger("condition", 0);
                }
                animator.SetFloat("Blend", 0);
            }

            // Changes the height position of the player..
            if (Input.GetButtonDown("Jump") && groundedPlayer)
            {
                StartCoroutine(JumpEnum());
            }

            if (Input.GetKeyDown(KeyCode.LeftControl) && crouched)
            {
                animator.SetBool("IsCrouching", false);
                crouched = false;
            }
            else if (Input.GetKeyDown(KeyCode.LeftControl) && !crouched)
            {
                animator.SetBool("IsCrouching", true);
                crouched = true;
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);
        }
        else
        {
            StartCoroutine(Dance());
        }
    }

    IEnumerator JumpEnum()
    {
        animator.SetBool("IsJumping", true);
        yield return new WaitForSeconds(0.5f);
        playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("IsJumping", false);
    }

    IEnumerator Dance()
    {
        animator.SetBool("IsDancing", true);
        mainCam.SetActive(false);
        danceCamera.SetActive(true);

        yield return new WaitForSeconds(16f);
        animator.SetBool("IsDancing", false);

        isDancing = false;

        danceCamera.SetActive(false);
        mainCam.SetActive(true);
    }
}
