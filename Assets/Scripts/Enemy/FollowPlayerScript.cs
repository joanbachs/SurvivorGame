﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayerScript : MonoBehaviour
{
    public Transform player;
    private NavMeshAgent enemy;
    public bool bDistance;
    private NavMeshPath path;

    [SerializeField] private GameObject preconfiguratedPlace;
    [SerializeField] protected AnimationsController ac;
    [SerializeField] private float speed;
    [SerializeField] private float maxDistance;
    [SerializeField] private float stopDistance;

    private bool follwingPlayer;

    // Use this for initialization
    public virtual void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player").GetComponent<Transform>();
        enemy.SetDestination(player.transform.position);
        path = new NavMeshPath();

        enemy.speed = speed;
        enemy.stoppingDistance = stopDistance;
        follwingPlayer = true;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        

        if (follwingPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                follwingPlayer = false;
            }

            if (Vector3.Distance(player.position, gameObject.transform.position) <= maxDistance)
            {
                bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
                Move(bDistance, player.transform);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                follwingPlayer = true;
            }

            if (Vector3.Distance(preconfiguratedPlace.transform.position, gameObject.transform.position) <= maxDistance)
            {
                bool bDistance = enemy.remainingDistance >= enemy.stoppingDistance;
                Move(bDistance, preconfiguratedPlace.transform);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ac.Attack();
        }
    }

    protected void Move(bool bDistance, Transform go)
    {
        if (!bDistance)
        {
            enemy.isStopped = true;
            ac.SetMovingState(false);
        }
        else
        {
            enemy.isStopped = false;
            enemy.SetDestination(go.position);
            ac.SetMovingState(true);
        }
    }
}
