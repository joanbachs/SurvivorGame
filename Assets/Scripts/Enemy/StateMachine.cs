﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateMachine : MonoBehaviour
{
    private NavMeshAgent enemy;
    private GameObject objective;
    [SerializeField] private float attackColdown;
    [SerializeField] private float idleColdown;
    [SerializeField] protected Animator ac;

    [SerializeField] private Transform[] patrollingPoints;
    private int patrollingIndex = 0;

    private Transform Player;
    private Vector3 distance;
    public enum states
    {
        Idle,
        Patrolling,
        FollowPlayer,
        Attack
    }

    public states currentState;

    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        currentState = states.Idle;
        Player = GameObject.Find("Player").GetComponent<Transform>();
    }

    void Update()
    {
        switch (currentState)
        {
            case states.Idle:
                Idle();
                break;

            case states.Patrolling:
                Patrolling();
                break;

            case states.FollowPlayer:
                FollowPlayer();
                break;

            case states.Attack:
                Attack();
                break;
        }

        float dist = Vector3.Distance(Player.position, transform.position);

        if (dist <= 1.5)
        {
            currentState = states.Attack;
        }
    }

    private void Idle()
    {
        idleColdown -= Time.deltaTime;

        if (idleColdown < 0)
        {
            currentState = states.Patrolling;
        }
    }

    private void Patrolling()
    {
        enemy.speed = 2f;

        if (!enemy.pathPending && enemy.remainingDistance < 0.5f)
            NextPatrollPoint();
    }

    private void FollowPlayer()
    {
        enemy.speed = 5.5f;
        ac.ResetTrigger("Attack");

        if (objective != null)
        {
            enemy.SetDestination(objective.transform.position);
        }
    }

    private void Attack()
    {
        attackColdown -= Time.deltaTime;

        if (attackColdown < 0)
        {
            ac.SetTrigger("Attack");
            attackColdown = 1f;
        }
    }

    private void NextPatrollPoint()
    {
        if (patrollingPoints.Length == 0) return;

        enemy.destination = patrollingPoints[patrollingIndex].position;

        patrollingIndex = (patrollingIndex + 1) % patrollingPoints.Length;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            currentState = states.FollowPlayer;
            objective = Player.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            currentState = states.Patrolling;
            objective = null;

        }
    }
}
