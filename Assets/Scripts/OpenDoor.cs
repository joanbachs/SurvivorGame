﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    private bool rotated;

    // Start is called before the first frame update
    void Start()
    {
        rotated = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.AccesToRestrictedArea)
        {
            if (!rotated)
            {
                gameObject.transform.Rotate(0f, 90f, 0f);
                rotated = true;
            }
            Debug.Log("acces granted");
        }
    }
}
