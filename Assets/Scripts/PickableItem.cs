﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItem : MonoBehaviour, IPickable
{
    private Color startcolor;
    private MeshRenderer renderer;

    private bool finished;
    public float interval;

    void Start()
    {
        if (gameObject.GetComponent<MeshRenderer>() != null)
        {
            renderer = gameObject.GetComponent<MeshRenderer>();
            startcolor = renderer.material.color;
        }

        InvokeRepeating("Highlight", 0, interval);
        finished = true;
    }

    void Highlight()
    {
        if (finished)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(1f,0.8f,0.2f,1f);
            finished = false;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material.color = startcolor;
            finished = true;
        }
    }

    public void OnPick()
    {
        throw new System.NotImplementedException();
    }
}
